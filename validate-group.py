#!/usr/bin/env python3

import gitlab
import gitlab.exceptions
import os
import argparse
import base64
import requests
import sys
from junit_xml import TestSuite, TestCase

class Validator():

    gl = None
    group = None
    groups = []

    def __init__(self, args):
        self.gl = gitlab.Gitlab("https://gitlab.com/", private_token=args.token)
        self.group = self.gl.groups.get(args.group)

    def validate(self):
        #check group visibility
        private_projects = self.group.projects.list(include_subgroups=True, visibility="private", as_list=False)
        privates = []
        errors = {"private":[],"license_missing":[]}

        print("Checking for private groups in %s" % self.group.path)
        self.get_subgroups(self.group)
        for group in self.groups:
            if group.attributes["visibility"] == "private":
                print("ERROR: Found private group: %s" % group.attributes["full_path"])
                privates.append(group.attributes["full_path"])

        print("Checking for private projects in %s" % group.path)
        for private in private_projects:
            print("ERROR: Found private project: %s" % private.attributes["path_with_namespace"])
            privates.append(private.attributes["path_with_namespace"])

        if privates:
            errors["private"] = privates

        if not os.path.exists("license_files"):
            os.makedirs("license_files")

        print("Checking for license files in %s" % group.path)
        projects = self.group.projects.list(include_subgroups=True, as_list=False)
        licenses = {}
        for project in projects:
            project_object = self.gl.projects.get(project.id)
            items = None
            try:
                items = project_object.repository_tree()
            except (gitlab.exceptions.GitlabHttpError, gitlab.exceptions.GitlabGetError):
                print("Can't fetch repo of %s" % project_object.attributes["path_with_namespace"])
            if items:
                license_file = self.find_license_file(items)
                if not license_file:
                    errors["license_missing"].append(project_object.attributes["path_with_namespace"])
                else:
                    license_type = self.write_license_file(project_object, license_file)
                    licenses[project_object.attributes["path_with_namespace"]] = license_type
        print("Found %s license files in %s projects." % (len(licenses), len(projects)))
        print("Writing reports")
        return self.report(errors, licenses)

    def get_subgroups(self, group):
        self.groups.append(group)
        subgroups = group.subgroups.list()
        for subgroup in subgroups:
            subgroup_object = self.gl.groups.get(subgroup.id)
            self.get_subgroups(subgroup_object)

    def find_license_file(self, repo_tree):
        license = None
        for item in repo_tree:
            if item["name"] == "LICENSE":
                found_license = True
                return item

    def write_license_file(self, project_object, license_file_metadata):
        file_info = project_object.repository_blob(license_file_metadata["id"])
        content = base64.b64decode(file_info['content']).decode("utf-8")
        try:
            with open("license_files/%s_LICENSE" % project_object.attributes["path_with_namespace"].replace("/","_"), "w+") as f:
                f.write(content)
        except:
            print("Could not write license file: %s_LICENSE" % project_object.attributes["path_with_namespace"].replace("/","_"))
        return content[0:content.find("\n")]

    def report(self, errors, licenses):
        reportfile = "OSS_report.md"
        failed = False
        with open(reportfile, "w") as report:
            report.write("# GitLab Open-Source Program validation report\n")
            if licenses:
                report.write("## Licenses\n")
                report.write("Please check if these licenses are OSI approved (https://opensource.org/licenses/category):\n\n")
                report.write("| project | license | file |\n")
                report.write("|---|---|---|\n")
                for project, license in licenses.items():
                    report.write("| %s | %s | license_files/%s_LICENSE)\n" % (project, license, project.replace("/","_")))
            if errors["private"] or errors["license_missing"]:
                failed = True
                report.write("\n## Errors\n")
                report.write("The script found some errors, please correct and verify these to conform to GitLab's Open-Source Program guidelines\n\n")
            if errors["license_missing"]:
                report.write("\n### License files missing\n")
                report.write("The script could not find `LICENSE` files in some projects. Make sure all projects contain `LICENSE` files with OSI approved licenses\n\n")
                for license_missing in errors["license_missing"]:
                    report.write("%s\n" % license_missing)
            if errors["private"]:
                report.write("\n### Private groups or projects\n")
                report.write("These private groups or projects are incompatible with GitLab's Open-Source Program guidelines!\n\n")
                for private in errors["private"]:
                    report.write("%s\n" % private)
        testsuite = self.make_test_suite(errors)
        with open("open_source_validation_report.xml","w") as junitfile:
            junitfile.write(TestSuite.to_xml_string([testsuite]))
        return failed

    def make_test_suite(self, errors):
        test_cases = []
        for private in errors["private"]:
            name = "private project or group"
            classname = private
            case = TestCase(name, classname)
            case.add_failure_info("%s is private. All projects should be public." % private)
            case.status = "fail"
            test_cases.append(case)
        for license_missing in errors["license_missing"]:
            name = "license missing"
            classname = license_missing
            case = TestCase(name, classname)
            case.add_failure_info("No LICENSE found in %s. All projects need an OSI approved license." % license_missing)
            case.status = "fail"
            test_cases.append(case)
        return TestSuite("Open-Source Program validation", test_cases)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Validate a GitLab.com group fulfills GitLab OSS program requirements')
    parser.add_argument('token', help='API token able to read the requested projects')
    parser.add_argument('group', help='Project ID to crawl for issues')

    args = parser.parse_args()
    validator = Validator(args)
    fail = validator.validate()
    if fail:
        exit(1)
