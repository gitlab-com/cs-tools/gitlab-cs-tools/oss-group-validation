# GitLab Open-Source Program requirements validation

Validates that a GitLab.com group fulfills the requirements of the [GitLab Open-Source Program](https://about.gitlab.com/solutions/open-source/program/)

## Features:

* Checks for a GitLab.com group:
  * If all subgroups and projects are public
  * If all projects contain `LICENSE` files
* Writes a report containing:
  * The first line of all `LICENSE` files found, for easy validation of OSI approved licenses
  * The complete license file for each project as a fallback 
  * all errors in form of private projects or projects without `LICENSE` files
* Documents errors in GitLab CI test cases

## Usage

* Fork the project
* Get an `api` scope API token (must be an owner of the group you want to validate)
* Configure the token in Settings->CI/CD variables:
  * Type: `Variable`
  * Name: `GIT_TOKEN`
  * set it to `masked`
* Configure `GROUP_ID` in `.gitlab-ci.yml` to your group's id
* run the pipeline

* you will find your report in the latest CI/CD->Pipelines artifacts